# -*- coding: utf-8 -*-

#------------------------------------------------------------------------------------------
# Programa creado por Juan Miguel Segura Fernandez
# Para contactar : jusefer2@hotmail.es
# LICENCIA GNU
#------------------------------------------------------------------------------------------

"""
ESTE PROGRAMA SIRVE PARA GUARDAR LOS USUARIOS Y LAS CONTRASEÑAS DE DIFERENTES LUGARES
"""

from Tkinter import *
from Tkinter import Tk
from PIL import Image, ImageTk # pip install Image / # Para importar el modulo de imagenes en WINDOWS
import tkMessageBox, os, time, sqlite3, hashlib, ttk, wget, urllib2, zipfile
from tkSimpleDialog import askstring

#--- Se valida el usuario y la contraseña ---
def Validar():
    usuario=(caja_usuario.get())
    contrasena_principal=(caja_contrasena.get())

    #ESTABLECE CONEXION CON EL TXT CON LOS USUARIOS
    f=open("Users.fsf","r")

    # Defino los usuarios por lineas
    l1=f.next()
    usuario0=f.next()
    usuario1=f.next()
    usuario2=f.next()

    #LE QUITO EL SALTO DE LINEA (\n) PARA QUE NO ME DE ERROR DE QUE NO ES IGUAL AL usuario_validar
    usuario0=usuario0.replace("\n","")
    usuario1=usuario1.replace("\n","")
    usuario2=usuario2.replace("\n","")

    # Encripto el usuario y miro si da lo mismo que el txt
    usuario = hashlib.md5(usuario).hexdigest()

    # Solo activar para probar en desarollo del proyecto
    # interfaz_validar.destroy()
    # menu(usuario, contrasena_principal)

    # Si el usuario es correcto
    if usuario==usuario0 or usuario==usuario1 or usuario==usuario2:
        # Cojo el nombre de usuario para ponerlo en la pantalla principal
        usuario=(caja_usuario.get())
        # Guardo en una variable los ficheros que se encuentran en el directorio ( para saber si existe la base de datos)
        ruta = os.getcwd()
        dir = os.listdir(ruta)
        # Si se deja en blanco la contraseña, para que gpg no se quede bugueado:
        if contrasena_principal=="":
            tkMessageBox.showerror("ERROR","No has introducido contraseña.")

        else:
            # Descifro la base de datos
            os.system("GnuPG\gpg.exe --batch --passphrase %s Datos_Usuarios.s3db.gpg" %(contrasena_principal))

            # Si la contraseña es correcta
            try:
                # Me conecto con la base de datos
                con=sqlite3.connect("Datos_Usuarios.s3db")
                cursor=con.cursor()
                con.text_factory=str
                # Ejecuto un sql para ver si se puede conectar
                sql="select * from Usuarios"
                cursor.execute(sql)
                con.close()
                os.system("del Datos_Usuarios.s3db.gpg") # Elimino la bd cifrada
                interfaz_validar.destroy() # Destruyo la interfaz de la validacion
                menu(usuario, contrasena_principal)

            # Si no se puede conectar con la base de datos
            except sqlite3.OperationalError:
                con.close() # Cierro la conexion establecida enteriormente
                # Si la contraseña es incorrecta
                if "Datos_Usuarios.s3db.gpg" in dir:
                    tkMessageBox.showerror("ERROR","Contraseña incorrecta.")
                    os.system("del Datos_Usuarios.s3db") # Borro la base de datos que se crea sola
                # Si la base de datos no existe en el directorio
                else:
                    tkMessageBox.showerror("ERROR","La base de datos no existe!\nContacta con el administrador.")
                    os.system("del Datos_Usuarios.s3db") # Borro la base de datos que se crea sola

    # Si el usuario no es correcto.
    else:
        tkMessageBox.showerror("ERROR","Nombre de usuario incorrecto.")

# Funcion que llama a la funcion Validar ( Esto es para cuando se da intro en las cajas en la interfaz validar)
def Validar_entry(event):
    Validar()

# Funcion que valida si el usuario es correcto o no cuando se clicka en la caja de la contraseña
def Validar_usuario(event):
    usuario=(caja_usuario.get())
    # Abro txt
    f=open("Users.fsf","r")

    # Defino los usuarios por lineas
    l1=f.next()
    usuario0=f.next()
    usuario1=f.next()
    usuario2=f.next()

    #LE QUITO EL SALTO DE LINEA (\n) PARA QUE NO ME DE ERROR DE QUE NO ES IGUAL AL usuario_validar
    usuario0=usuario0.replace("\n","")
    usuario1=usuario1.replace("\n","")
    usuario2=usuario2.replace("\n","")
    # Encripto el usuario y miro si da lo mismo que el txt
    usuario = hashlib.md5(usuario).hexdigest()

    # Si el usuario coincide
    if usuario == usuario0 or usuario == usuario1 or usuario == usuario2:
        # Creo una label sin texto para tapar la label de usuario incorrecto
        # --- MUY CUTRE TIO ---
        label_vacia=Label(interfaz_validar, text="\t\t\t\t", fg="#3e8de7", bg="#3e8de7")
        label_vacia.pack()
        label_vacia.place(x=520, y=148)

    # Si no coincide el usuario pongo una label
    else:
        # Creo una label de incorrecto
        label_usuario_incorrecto=Label(interfaz_validar, text="Usuario incorrecto.", fg="#950000", bg="#3e8de7")
        label_usuario_incorrecto.pack()
        label_usuario_incorrecto.place(x=520, y=148)

    f.close()
# Menu de opciones
def menu(usuario, contrasena_principal):
    # Creo la interfaz del menu principal
    interfaz_menu=Tk()
    interfaz_menu.title("Menú")
    interfaz_menu.geometry("700x380+{}+{}".format(positionRight, positionDown))
    bit = interfaz_menu.iconbitmap("img/icono.ico")
    interfaz_menu.resizable(0,0)

    # Label del menu
    label_welcome=Label(interfaz_menu, text="Menú principal", font="Helveltica 18 italic", fg="blue", height=3)
    label_welcome.pack()
    label_welcome.place(x=260, y=12)

    # Logo del programa arriba a la derecha
    logo=ImageTk.PhotoImage(file="img/icono_menu.png")
    label_logo=Label(interfaz_menu, image=logo)
    label_logo.pack()
    label_logo.place(x=5, y=-10)

    # Label de usuario
    label_usuario=Label(interfaz_menu, text="Usuario: %s" %(usuario))
    label_usuario.pack()
    label_usuario.place(x=580, y=10)

    # Label de hora
    hora=time.strftime("%H:%M:%S")
    label_hora=Label(interfaz_menu, text="Hora: %s" %(hora))
    label_hora.pack()
    label_hora.place(x=580, y=30)

    # Label de fecha
    fecha=time.strftime("%d/%m/%y")
    label_fecha=Label(interfaz_menu, text="Fecha: %s"%(fecha))
    label_fecha.pack()
    label_fecha.place(x=580, y=50)


    canvas = Canvas(interfaz_menu, bg="blue", width=700, height=1.5)
    canvas.pack()
    canvas.place(x=0, y=110)

    # Funcion agregar
    def agregar():
        interfaz_menu.destroy() # Destruyo la interfaz menu
        # Defino la interfaz agregar
        interfaz_agregar=Tk()
        interfaz_agregar.title("Agregar datos")
        interfaz_agregar.geometry("700x400+{}+{}".format(positionRight, positionDown))
        interfaz_agregar.resizable(0,0)
        bit = interfaz_agregar.iconbitmap("img/icono.ico")

        # Label del menu de agregar datos
        label_interfaz_agregar=Label(interfaz_agregar, text="Agregar datos nuevos.", font="Helveltica 15 underline", fg="blue")
        label_interfaz_agregar.pack()
        label_interfaz_agregar.place(x=20, y=20)

        # Label y caja de lugar
        label_lugar=Label(interfaz_agregar, text="Lugar:", font="Helveltica 13")
        label_lugar.pack()
        label_lugar.place(x=25, y=80)
        caja_lugar=ttk.Entry(interfaz_agregar, width=24, font="Helveltica 11")
        caja_lugar.pack()
        caja_lugar.place(x=90, y=82)

        # Label y caja de usuario
        label_usuario=Label(interfaz_agregar, text="Usuario:", font="Helveltica 13")
        label_usuario.pack()
        label_usuario.place(x=25, y=130)
        caja_usuario=ttk.Entry(interfaz_agregar, width=23, font="Helveltica 11")
        caja_usuario.pack()
        caja_usuario.place(x=100, y=132)

        # Label y caja de contraseña
        label_contrasena=Label(interfaz_agregar, text="Contraseña:", font="Helveltica 13")
        label_contrasena.pack()
        label_contrasena.place(x=25, y=180)
        caja_contrasena=ttk.Entry(interfaz_agregar, width=20, font="Helveltica 11", show="*")
        caja_contrasena.pack()
        caja_contrasena.place(x=125, y=182)

        # Funcion para ver la contraseña en la caja
        def ver_pass():
            caja_contrasena.config(show="")
            # Oculto el boton de ver contraseña
            boton_ver_contrasena.place_forget()
            boton_imagen_ocultar_contrasena.place(x=300, y=185)
        # Imagen y boton ver contraseña
        imagen_ver_contrasena=ImageTk.PhotoImage(file="img/ver_contrasena.png")
        boton_ver_contrasena=Button(interfaz_agregar, image=imagen_ver_contrasena, command=ver_pass, borderwidth=0, cursor="hand2")
        boton_ver_contrasena.pack()
        boton_ver_contrasena.place(x=300, y=185)

        # Funcion para ocultar la contraseña poniendo show * a la caja_contrasena
        def ocultar_pass():
            caja_contrasena.config(show="*")
            boton_imagen_ocultar_contrasena.place_forget()
            boton_ver_contrasena.place(x=300, y=185)
        # Imagen y boton para ocultar la contraseña
        imagen_ocultar_contrasena=ImageTk.PhotoImage(file="img/ocultar_contrasena.png")
        boton_imagen_ocultar_contrasena=Button(interfaz_agregar, image=imagen_ocultar_contrasena, command=ocultar_pass, borderwidth=0, cursor="hand2")
        boton_imagen_ocultar_contrasena.pack()
        boton_imagen_ocultar_contrasena.forget()


        # Funcion que guarda los datos en la base de datos
        def guardar_datos():
            # Defino los datos introducidos en las cajas del menu agregar
            lugar_agregar=(caja_lugar.get())
            usuario_agregar=(caja_usuario.get())
            contrasena_agregar=(caja_contrasena.get())

            # Si no se introduce ningun dato
            if lugar_agregar=="" and usuario_agregar=="" and contrasena_agregar=="":
                tkMessageBox.showerror("ERROR","No se ha intruducido ningun dato.")

            # Si se ha intruducido datos ( LO NORMAL )
            else:
                try:
                    # Me conecto a la base de datos
                    con=sqlite3.connect("Datos_Usuarios.s3db")
                    cursor=con.cursor()
                    # Introduzo los datos en la BD
                    cursor.execute("insert into Usuarios (Lugar, Usuario, Contrasena) values (?, ?, ?)", (lugar_agregar, usuario_agregar, contrasena_agregar))
                    con.commit()
                    con.close() #cerrar la conexion
                    # Si los datos se han guardado
                    if cursor.execute:
                        tkMessageBox.showinfo("Agregado.","Los datos han sido agregados con éxito.")
                    # Si no se ha guardado
                    else:
                        tkMessageBox.showerror("ERROR","Error al guardar los datos. Intentelo de nuevo.")

                # Si ya existen los datos
                except sqlite3.IntegrityError:
                    tkMessageBox.showerror("ERROR","Ya existen datos guardados de '%s'" %(lugar_agregar))

        def contrasena_segura(event):
            import random

            # Defino la longitud de la contraseña y los valores que va a utilizar
            longitud = 16
            valores = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ<=>@#%&+"

            # Defino la lista para añadirle los caracteres random
            contrasena_segura=[]
            p1=""

            # Creo un bucle para que escoja los caracteres random y los añada en la lista de password
            for i in range (0, longitud):
                p1=random.choice(valores)
                contrasena_segura.append(p1)

            # Escribo en un txt la contraseña en modo escritura
            f=open("c.txt","w")
            f.write(str(contrasena_segura))
            f.close()

            # Abro el txt en modo lectura y quito los caracteres raros que salen en la lista
            f=open("c.txt","r")
            contrasena_segura=f.readline()
            contrasena_segura=contrasena_segura.replace("[","")
            contrasena_segura=contrasena_segura.replace("]","")
            contrasena_segura=contrasena_segura.replace(",","")
            contrasena_segura=contrasena_segura.replace(" ","")
            contrasena_segura=contrasena_segura.replace("'","")
            f.close()
            # Elimino el archivo (IMPORTANTE)
            os.system("del c.txt")

            # Borro lo que hay en caja_contrasena
            caja_contrasena.delete(0, 'end')
            # Inserto en la caja_contrasena la contraseña segura
            caja_contrasena.insert(END, contrasena_segura)



        # Label y boton para generar contraseña segura
        label_contraena_segura=Label(interfaz_agregar, text="¿Deseas crear una contraseña segura?", font="Helvetica 12 italic underline", fg="#006600", cursor="hand2")
        label_contraena_segura.pack()
        label_contraena_segura.place(x=25, y=215)
        label_contraena_segura.bind("<Button-1>", contrasena_segura)

        # Boton para guardar los datos en la base de datos
        imagen_boton_guardar=ImageTk.PhotoImage(file="img/guardar.png")
        boton_guardar_agregar=ttk.Button(interfaz_agregar, image=imagen_boton_guardar, cursor="hand2", text="Guardar datos", command=guardar_datos, compound="left", style="boton_guardar_agregar.TButton", width=28)
        ttk.Style().configure("boton_guardar_agregar.TButton",  foreground="#0a6dbe", font="Helveltica 11")
        boton_guardar_agregar.pack()
        boton_guardar_agregar.place(x=20, y=260)

        # Label y imagen del icono
        imagen_icono=ImageTk.PhotoImage(file="img/icono.png")
        label_imagen_icono=Label(interfaz_agregar, image=imagen_icono)
        label_imagen_icono.pack()
        label_imagen_icono.place(x=400)


        # Imagen, funcion y boton volver
        def volver_agregar():
            interfaz_agregar.destroy()
            menu(usuario, contrasena_principal)
        imagen_volver=ImageTk.PhotoImage(file="img/volver.png")
        boton_volver=Button(interfaz_agregar, image=imagen_volver, cursor="hand2", borderwidth=0, command=volver_agregar)
        boton_volver.pack()
        boton_volver.place(x=25, y=315)
        # Para que al pulsar la X llame a volver_agregar
        interfaz_agregar.protocol("WM_DELETE_WINDOW", volver_agregar)
        interfaz_agregar.mainloop()

    # Funcion para modificar los datos
    def modificar():
        # Askstring que pregunta que el nombre del lugar del que desea modificar la información
        lugar = askstring("Modificar datos", "Escribe el nombre del 'Lugar' del que deseas modificar la información.")
        # Interfaz de modificar
        interfaz_modificar2=Toplevel()
        interfaz_modificar2.title("Modificar datos")
        interfaz_modificar2.geometry("700x390+{}+{}".format(positionRight, positionDown))
        interfaz_modificar2.resizable(0,0)
        bit = interfaz_modificar2.iconbitmap("img/icono.ico")

        # Imagen y label del icono del programa
        imagen_icono=ImageTk.PhotoImage(file="img/icono.png")
        label_imagen_icono=Label(interfaz_modificar2, image=imagen_icono)
        label_imagen_icono.pack()
        label_imagen_icono.place(x=400)

        # Label del menu de modificar datos
        label_interfaz_agregar=Label(interfaz_modificar2, text="Modificar datos", height=1, font="Helveltica 15 underline", fg="blue")
        label_interfaz_agregar.pack()
        label_interfaz_agregar.place(x=25, y=30)

        # Si no se ha introducido ningun dato en la caja del lugar
        if lugar=="":
            interfaz_modificar2.destroy()
            tkMessageBox.showerror("ERROR","No se ha introducido ningun dato a buscar.")

        # Si se ha dado a "cancel" en el askstring
        elif lugar == None:
            interfaz_modificar2.destroy()

        # Si se han intruducido datos en la caja de lugar ( LO NORMAL )
        else:
            # Abro conexion con la base de datos
            con=sqlite3.connect("Datos_Usuarios.s3db")
            cursor=con.cursor()
            con.text_factory=str
            # Para comprobar si existe ese Lugar
            sql="select * from Usuarios where Lugar like '%%%s%%'" %(lugar)
            cursor.execute(sql)
            existe=False # Variable para comprobar que existe el lugar introducido
            # Miro a ver si hay mas de un dato
            # Label y caja de lugar
            label_lugar=Label(interfaz_modificar2, text="Lugar:", font="Helveltica 12 italic" )
            label_lugar.pack()
            label_lugar.place(x=25, y=70)
            caja_lugar_modificar=ttk.Entry(interfaz_modificar2, width=15, font="Helveltica 11")
            caja_lugar_modificar.pack()
            caja_lugar_modificar.place(x=25, y=100)

            # Label y caja de usuario
            label_usuario=Label(interfaz_modificar2, text="Usuario:", font="Helveltica 12 italic")
            label_usuario.pack()
            label_usuario.place(x=25, y=140)
            caja_usuario=ttk.Entry(interfaz_modificar2, width=25, font="Helveltica 11")
            caja_usuario.pack()
            caja_usuario.place(x=25, y=170)

            # Label y caja de contraseña
            label_contrasena=Label(interfaz_modificar2, text="Contraseña:", font="Helveltica 12 italic")
            label_contrasena.pack()
            label_contrasena.place(x=25, y=210)
            caja_contrasena=ttk.Entry(interfaz_modificar2, width=25, font="Helveltica 11")
            caja_contrasena.pack()
            caja_contrasena.place(x=25, y=240)
            existe = False
            registro = 0
            # Creo una lista para meter los datos
            lista_usuarios = []
            # Añado los datos a una lista
            for Usuarios in cursor:
                # Muestro solo el primer registro
                if registro == 0:
                    # Inserto los datos de la base de datos en las cajas
                    caja_lugar_modificar.insert(END, "%s" %(Usuarios[0]))
                    caja_usuario.insert(END, "%s" %(Usuarios[1]))
                    caja_contrasena.insert(END, "%s" %(Usuarios[2]))
                # Añado los demas registros excepto el primero en una lista
                else:
                    # Inserto todo en una lista
                    lista_usuarios.append(Usuarios[0])
                    lista_usuarios.append(Usuarios[1])
                    lista_usuarios.append(Usuarios[2])
                # Variables de que existe y de que no es el primer registro
                existe = True
                registro = registro + 1

            # Si los datos consultados no existen
            if existe == False:
                con.close()
                interfaz_modificar2.destroy()
                tkMessageBox.showerror("Error", "Los datos consultados no existen.")

            # Funcion para pasar al siguiente dato de la base de datos
            def next():
                # Mientras hayan datos en la lista
                if lista_usuarios != []:
                    # Limpio los datos que hayan en las cajas
                    caja_lugar_modificar.delete(0, 'end')
                    caja_usuario.delete(0, 'end')
                    caja_contrasena.delete(0, 'end')
                    # Inserto los datos de la lista en las cajas
                    caja_lugar_modificar.insert(END, "%s" %(lista_usuarios[0]))
                    caja_usuario.insert(END, "%s" %(lista_usuarios[1]))
                    caja_contrasena.insert(END, "%s" %(lista_usuarios[2]))
                    # Elimino de la lista los elementros mostrados
                    lista_usuarios.remove(lista_usuarios[0])
                    lista_usuarios.remove(lista_usuarios[0])
                    lista_usuarios.remove(lista_usuarios[0])

                # Si no hay mas registros en la lista
                else:
                    label_no_mas=Label(interfaz_modificar2, text="No hay mas registros.", fg="red")
                    label_no_mas.pack()
                    label_no_mas.place(x=25, y=315)
                    # Llamo despues de un segundo a esta funcion que elimina la label_no_mas
                    def destroy_label_nomas():
                        label_no_mas.destroy()
                    label_no_mas.after(1000, destroy_label_nomas)


            # Si hay mas de un registro creo un boton que vuelva a hacer el for
            # Imagen y boton siguiente
            imagen_boton_siguiente=ImageTk.PhotoImage(file="img/next.png")
            boton_siguiente=ttk.Button(interfaz_modificar2, image=imagen_boton_siguiente, compound="right", text="Siguiente", command=next, width=13)
            boton_siguiente.pack()
            boton_siguiente.place(x=25, y=285)

            # Funcion para guardar los datos modificados
            def guardar_modificar():
                # Defino los datos introducidos en las cajas de modificar
                 lugar_modificar=(caja_lugar_modificar.get())
                 usuario_modificar=(caja_usuario.get())
                 contrasena_modificar=(caja_contrasena.get())
                 sql="Update Usuarios set Lugar='%s', Usuario='%s', Contrasena='%s' where Lugar='%s'" %(lugar_modificar, usuario_modificar, contrasena_modificar, lugar_modificar)
                 cursor.execute(sql)
                 con.commit()
                 # Si se ha ejecutado el cursor
                 if (cursor.execute(sql)):
                     interfaz_modificar2.destroy()
                     tkMessageBox.showinfo("Modificado con éxito.","Los datos de '%s' se han actualizado correctamente." %(lugar_modificar))

                 # Si ha habido un error al ejecutarse el sql
                 else:
                     interfaz_modificar2.destroy()
                     tkMessageBox.showerror("ERROR", "Ha habido un error al guardar los datos de '%s'" %(lugar_modificar))

                 con.close()

            # Boton y imagen para guardar los datos modificados
            imagen_guardar_modificar=ImageTk.PhotoImage(file="img/guardar.png")
            boton_guardar_modificar=ttk.Button(interfaz_modificar2, text="Guardar cambios", image=imagen_guardar_modificar, compound="left", width=22, cursor="hand2",  command=guardar_modificar)
            boton_guardar_modificar.pack()
            boton_guardar_modificar.place(x=220, y=335)

            # Boton y funcion volver en modificar
            def volver_modificar2():
                con.close()
                interfaz_modificar2.destroy()
            imagen_volver=ImageTk.PhotoImage(file="img/volver.png")
            boton_volver_modificar=Button(interfaz_modificar2, cursor="hand2", image=imagen_volver, command=volver_modificar2, borderwidth=0)
            boton_volver_modificar.pack()
            boton_volver_modificar.place(x=25, y=325)
            interfaz_modificar2.protocol("WM_DELETE_WINDOW", volver_modificar2) # Para volver atras con la X

        # Cierro la interfaz
        interfaz_modificar2.mainloop()


    #Funcion para consultar datos
    def consultar_interfaz():
        # Destruyo la interfaz del menu
        interfaz_menu.destroy()

        # Inicio una nueva interfaz
        interfaz_consultar=Tk()
        interfaz_consultar.title("Consultar datos")
        interfaz_consultar.geometry("700x390+{}+{}".format(positionRight, positionDown))
        interfaz_consultar.resizable(0,0)
        bit = interfaz_consultar.iconbitmap("img/icono.ico")

        # Label del icono del programa
        imagen_icono=ImageTk.PhotoImage(file="img/icono.png")
        label_imagen_icono=Label(interfaz_consultar, image=imagen_icono)
        label_imagen_icono.pack()
        label_imagen_icono.place(x=350, y=0)

        # Label titulo consultar
        label_consultar=Label(interfaz_consultar, text="Consultar datos.", fg="blue", font="Helvetica 15 underline", height=1)
        label_consultar.pack()
        label_consultar.place(x=25, y=15)

        # Label instrucciones
        label_consultar2=Label(interfaz_consultar, text="Escribe los parametros que deseas consultar.", font="Helvetica 11")
        label_consultar2.pack()
        label_consultar2.place(x=25, y=50)

        # Label y caja del lugar
        label_lugar=Label(interfaz_consultar, text="Lugar:", font="Helveltica 12 italic")
        label_lugar.pack()
        label_lugar.place(x=25, y=80)
        caja_lugar=ttk.Entry(interfaz_consultar, width=15, font="Helveltica 11")
        caja_lugar.pack()
        caja_lugar.place(x=25, y=110)

        # Label y caja de usuario
        label_usuario=Label(interfaz_consultar, text="Usuario:", font="Helveltica 12 italic")
        label_usuario.pack()
        label_usuario.place(x=25, y=140)
        caja_usuario=ttk.Entry(interfaz_consultar, width=25, font="Helveltica 11")
        caja_usuario.pack()
        caja_usuario.place(x=25, y=170)

        # Label y caja de contraseña
        label_contrasena=Label(interfaz_consultar, text="Contraseña:", font="Helveltica 12 italic")
        label_contrasena.pack()
        label_contrasena.place(x=25, y=200)
        caja_contrasena=ttk.Entry(interfaz_consultar, width=25, font="Helveltica 11")
        caja_contrasena.pack()
        caja_contrasena.place(x=25, y=230)

        def consultar():
            # DEFINO EL TEXTO INTRODUCIDO EN LAS CAJAS
            lugar_consultar=(caja_lugar.get())
            usuario_consultar=(caja_usuario.get())
            contrasena_consultar=(caja_contrasena.get())

            #Abro la conexion con la base de datos y defino el cursor
            con=sqlite3.connect("Datos_Usuarios.s3db")
            cursor=con.cursor()
            con.text_factory=str # Defino que los datos sacados de la base de datos sea una lista y no me de error

            # Consulta por caracteres de todos los parametros
            sql="select * from Usuarios where Lugar LIKE '%%%s%%' and Usuario LIKE '%%%s%%' and Contrasena LIKE '%%%s%%'"  %(lugar_consultar, usuario_consultar, contrasena_consultar)
            cursor.execute(sql)
            existe=False # Variable por si no existen los datos intruducidos

            # Creo una lista donde añado lo consultado en la base de datos
            lista_usuarios = []
            # Consulto todos los datos
            for Usuarios in cursor:
                existe = True
                # Añado a la lista el lugar, usuario y contraseña
                lista_usuarios.append(Usuarios[0])
                lista_usuarios.append(Usuarios[1])
                lista_usuarios.append(Usuarios[2])

            # Si no existen los datos intruducidos
            if existe==False:
                # Destruyo la interfaz en la que sale el resultado de la busqueda
                tkMessageBox.showerror("ERROR", "No hay ningun registro introducido con esos parametros.")
                con.close()

            # Elimino el texto que se haya escrito en las cajas
            caja_lugar.delete(0, "end")
            caja_usuario.delete(0, "end")
            caja_contrasena.delete(0, "end")
            # Añado el primer registro en las cajas de la interfaz consultar
            caja_lugar.insert(END, lista_usuarios[0])
            caja_usuario.insert(END, lista_usuarios[1])
            caja_contrasena.insert(END, lista_usuarios[2])
            # Elimino de la lista los datos del primer registro que he añadido a las cajas
            lista_usuarios.remove(lista_usuarios[0])
            lista_usuarios.remove(lista_usuarios[0])
            lista_usuarios.remove(lista_usuarios[0])
            # Cierro conexion con DB
            con.close()

            # Funcion para ver el siguiente registro
            def next():
                # Si quedan datos en la lista
                if lista_usuarios != []:
                    # Elimino el texto que se haya escrito en las cajas
                    caja_lugar.delete(0, "end")
                    caja_usuario.delete(0, "end")
                    caja_contrasena.delete(0, "end")
                    # Añado el primer registro en las cajas de la interfaz consultar
                    caja_lugar.insert(END, lista_usuarios[0])
                    caja_usuario.insert(END, lista_usuarios[1])
                    caja_contrasena.insert(END, lista_usuarios[2])

                    # Elimino de la lista los datos del primer registro que he añadido a las cajas
                    lista_usuarios.remove(lista_usuarios[0])
                    lista_usuarios.remove(lista_usuarios[0])
                    lista_usuarios.remove(lista_usuarios[0])
                # Si no hay mas registros en la lista
                else:
                    # Label de que no quedan mas regsitros
                    label_no_mas=Label(interfaz_consultar, text="No hay mas registros.", fg="red")
                    label_no_mas.pack()
                    label_no_mas.place(x=25, y=300)
                    # Llamo despues de un segundo a esta funcion que elimina la label_no_mas
                    def destroy_label_nomas():
                        label_no_mas.destroy()
                    label_no_mas.after(1500, destroy_label_nomas)

            # Oculto el boton de consultar datos
            boton_consultar.place_forget()
            # Boton para ver el siguiente registro
            boton_siguiente=ttk.Button(interfaz_consultar, image=imagen_boton_siguiente, compound="right", text="Siguiente", command=next, width=13)
            boton_siguiente.pack()
            boton_siguiente.place(x=25, y=270)

            # Funcion para volver a consultar datos nuevos
            def consultar_de_nuevo():
                # Elimino el texto que se haya escrito en las cajas
                caja_lugar.delete(0, "end")
                caja_usuario.delete(0, "end")
                caja_contrasena.delete(0, "end")
                # Oculto el boton de siguiente y de volver a consultar
                boton_siguiente.place_forget()
                boton_consultar_denuevo.place_forget()
                # Muestro el boton de consultar
                boton_consultar.place(x=25, y=270)

            # Boton para volver a consultar datos
            boton_consultar_denuevo=ttk.Button(interfaz_consultar, text="Consultar datos nuevos  ", compound="left", image=imagen_boton_consultar, command=consultar_de_nuevo)
            boton_consultar_denuevo.pack()
            boton_consultar_denuevo.place(x=150, y=270)

        # Boton consultar en la interfaz consultar
        imagen_boton_consultar=ImageTk.PhotoImage(file="img/img_boton_consultar.png")
        boton_consultar=ttk.Button(interfaz_consultar, command=consultar, text="Consultar datos", cursor="hand2", width=18, image=imagen_boton_consultar, compound="left", style="boton_consultar.TButton")
        ttk.Style().configure("boton_consultar.TButton", foreground="blue", font="Helveltica 11 italic")
        boton_consultar.pack()
        boton_consultar.place(x=25, y=270)
        # Imagen para ver el siguiente registro
        imagen_boton_siguiente=ImageTk.PhotoImage(file="img/next.png")
        # Funcion que destruye la interfaz actual y vuelve a la funcion menu
        def volver_consultar():
            interfaz_consultar.destroy()
            menu(usuario, contrasena_principal)
        # Para volver atras con la X
        interfaz_consultar.protocol("WM_DELETE_WINDOW", volver_consultar)

        # Imagen del boton volver consultar
        imagen_boton_volver_consultar=ImageTk.PhotoImage(file="img/volver.png")
        # Boton para volver a la interfaz_menu
        boton_volver_consultar=Button(interfaz_consultar, command=volver_consultar, image=imagen_boton_volver_consultar, borderwidth=0, cursor="hand2")
        boton_volver_consultar.pack()
        boton_volver_consultar.place(x=25, y=315)

        interfaz_consultar.mainloop()

    def eliminar():
        lugar = askstring("Eliminar datos", "Escribe el nombre exacto del 'Lugar' del que deseas eliminar la información.")
        # Si no se ha introducido ningun dato en la caja del lugar
        if lugar=="":
            tkMessageBox.showerror("ERROR","No se ha introducido ningun dato a eliminar.")

        # Si se ha dado a "cancel" en el askstring
        elif lugar == None:
            pass

        # Si se introducen datos
        else:
            # Conectar con la base de datos
            con=sqlite3.connect("Datos_Usuarios.s3db")
            cursor=con.cursor()
            # Consulto
            sql=("select * from Usuarios where Lugar='%s'" %(lugar))
            cursor.execute(sql)
            existe = False
            for Usuario in cursor:
                existe = True
            # Si los datos consultados existen
            if existe == True:
                # Pregunto a ver si desea eliminar los datos
                eliminar=tkMessageBox.askquestion("Eliminar.", "Esta seguro de que desea eliminar los siguientes datos:\n\t-------------------------------------------\n\tLugar: %s.\n\tUsuario: %s.\n\tContrasena: No disponible.\n\t-------------------------------------------" %(Usuario[0], Usuario[1]))
                # Eliminar datos
                if eliminar=="yes":
                    # Si el numero ot introducido existe
                    sql="delete from Usuarios where Lugar='%s'"%(lugar)
                    cursor.execute(sql)
                    con.commit()
                    #Si se ha ejecutado cursor.execute(sql)
                    if (cursor.execute(sql)):
                        tkMessageBox.showinfo("Datos eliminados.","Los datos de '%s' se han eliminado correctamente." %(lugar))
                    # Si no se ejecuta, porque hay algun error
                    else:
                        tkMessageBox.showerror("ERROR", "Ha habido un error al eliminar los datos, pruebe de nuevo.")

                # Si se selecciona "no" en la pregunta de eliminar (eliminar)
                else:
                    tkMessageBox.showinfo("No eliminados.","Datos de '%s' no eliminados"%(lugar))

            # Si los datos no existen
            elif existe == False:
                tkMessageBox.showerror("Error.", "No existen datos del lugar %s." %(lugar))

            con.close() #Cierro la conexion con la base de datos

    # Funcion para salir del programa y cifrar la BD
    def salir():
        # Cifro la base de datos
        os.system("GnuPG\gpg.exe -c --batch --passphrase %s Datos_Usuarios.s3db" %(contrasena_principal))
        os.system("del Datos_Usuarios.s3db") # Elimino la base de datos descifrada
        sys.exit()

    # -----------------------------------
    # BOTONES DE LA INTERFAZ PRINCIPAL
    # -----------------------------------

    # ESTO ES PARA QUE CUANDO SE PULSA LA X DE LA PANTALLA PARA SALIR, SE EJECUTE LA FUNCION DE SALIR
    interfaz_menu.protocol("WM_DELETE_WINDOW", salir)

    # AGREGAR
    # Imagen del boton agregar
    imagen_agregar=ImageTk.PhotoImage(file="img/agregar.png")
    # Boton agregar
    boton_agregar=ttk.Button(interfaz_menu, command=agregar, cursor="hand2", image=imagen_agregar, text="Agregar datos", compound="top")
    boton_agregar.pack()
    boton_agregar.place(x=30, y=125)

    # MODIFICAR
    # Imagen de modificar
    imagen_modificar=ImageTk.PhotoImage(file="img/modificar.png")
    # Boton modificar en la interfaz principal
    boton_modificar=ttk.Button(interfaz_menu, command=modificar, cursor="hand2", image=imagen_modificar, text="Modificar datos", compound="top")
    boton_modificar.pack()
    boton_modificar.place(x=200, y=125)

    # CONSULTAR
    # Imagen de la lupa
    imagen_consultar=ImageTk.PhotoImage(file="img/lupa.png" )
    # Boton consultar de la interfaz principal
    boton_interfaz_consultar=ttk.Button(interfaz_menu,  command=consultar_interfaz, cursor="hand2", text="Consultar datos", compound="top")
    boton_interfaz_consultar.config(image=imagen_consultar)
    boton_interfaz_consultar.place(x=375, y=125)

    # ELIMINAR
    #Imagen del boton eliminar
    imagen_eliminar=ImageTk.PhotoImage(file="img/eliminar.png")
    # Boton eliminar de la interfaz principal
    boton_eliminar=ttk.Button(interfaz_menu, command=eliminar, cursor="hand2", image=imagen_eliminar, text="Eliminar datos", compound="top")
    boton_eliminar.pack()
    boton_eliminar.place(x=550, y=125)

    # SALIR DE LA INTERFAZ PRINCIPAL
    imagen_salir=ImageTk.PhotoImage(file="img/exit_interfaz_principal.png")
    boton_salir=ttk.Button(interfaz_menu, command=salir, image=imagen_salir, cursor="hand2")
    boton_salir.pack()
    boton_salir.place(x=15, y=285)

    # FUNCION PARA ACTUALIZAR EL PROGRAMA
    def update():
        # Miro si la version del programa es la misma que la que hay subida en gitlab
        f = urllib2.urlopen("https://gitlab.com/jmseguraf01/usuarios-windows/blame/master/version")
        lee = f.readlines()
        f.close()
        # Leo por linea y busco la palabra plaintext que es donde pone la version
        for line in lee:
            if "plaintext" in line:
                version_gitlab = line
        # Substituyo lo que no es la version
        version_gitlab = version_gitlab.replace("""<pre class="code highlight"><code><span id="LC1" class="line" lang="plaintext">""", "")
        version_gitlab = version_gitlab.replace("</span></code></pre>", "")
        version_gitlab = version_gitlab.replace("\n", "")

        # Variable version_txt es la version del programa que pone en el txt (..\version)
        # version_txt
        # Si la version es la misma
        if version_gitlab == version_txt:
            tkMessageBox.showerror("Sin actualizaciones.", "Actualmente tiene la última versión del programa.")

        # Si hay una actualizacion
        else:
            actualizar = tkMessageBox.askyesno("Actualización disponible.", "Desea actualizar el programa a la versión %s ?" %(version_gitlab))
            # Si se desea actualizar
            if actualizar == True:
                label_actualizando = Label(interfaz_menu, text="Descargando actualización... ", font="Helveltica 12 italic", fg="#005718")
                label_actualizando.pack()
                label_actualizando.place(x=250, y=300)
                # Funcion para descargar actualizacion
                def descargar_actualizacion():
                    # Descargo el zip del gitlab
                    wget.download("https://gitlab.com/jmseguraf01/usuarios-windows/-/archive/master/usuarios-windows-master.zip")
                    label_actualizando.config(text="Descargado con éxito.")
                    time.sleep(1.5)
                    label_actualizando.config(text="Descomprimiendo archivos...")
                    # Descomprimo la carpeta zip descargada
                    with zipfile.ZipFile("usuarios-windows-master.zip", 'r') as zip_ref:
                        zip_ref.extractall("usuarios-windows")
                    # Copio usuarios.py, version y img
                    os.system("copy usuarios-windows\\usuarios-windows-master\\bin\\usuarios.exe usuarios.exe")
                    os.system("copy usuarios-windows\\usuarios-windows-master\\version ..\\version")
                    os.system("xcopy /Y usuarios-windows\\usuarios-windows-master\\bin\\img img")
                    os.system("del usuarios-windows-master.zip")
                    os.system("rd /S /Q  usuarios-windows")
                    # Actualizado con exito
                    label_actualizando.config(text="Actualizado con éxito.\nCierre el programa y abralo de nuevo.")
                label_actualizando.after(2000, descargar_actualizacion)

            # No se actualiza
            elif actualizar == False:
                pass


    # Funcion para cambiar la contraseña de la base de datos
    def cambiar_contrasena(contrasena_principal):
        interfaz_menu.destroy()
        # Creo la nueva interfaz
        interfaz_cambiar_contrasena = Tk()
        interfaz_cambiar_contrasena.title("Cambiar contraseña")
        interfaz_cambiar_contrasena.geometry("650x380+{}+{}".format(positionRight, positionDown))
        bit = interfaz_cambiar_contrasena.iconbitmap("img/icono.ico")

        # Linea que separa la imagen del incio de sesion
        canvas = Canvas(interfaz_cambiar_contrasena, bg="white", width=700, height=100)
        canvas.pack()
        canvas.place(x=0, y=0)

        # Imagen y label del logo arriba a la izquierda
        imagen_logo = ImageTk.PhotoImage(file="img/icono_pequeno.ico", width=10)
        label_logo = Label(image=imagen_logo, bg="white")
        label_logo.pack()
        label_logo.place(x=10, y=0)

        # Label del titulo
        label_titulo = Label(interfaz_cambiar_contrasena, text="Cambio de contraseña", font="Helveltica 18", bg="white")
        label_titulo.pack()
        label_titulo.place(x=100, y=55)
        # Linea de debajo del titulo
        canvas.create_line(0, 100, 700, 100, fill="black", width=1.5)

        # Label contraseña antigua
        label_contrasena_antigua = Label(interfaz_cambiar_contrasena, text="Escriba la contraseña actual:", font="Helveltica 12")
        label_contrasena_antigua.pack()
        label_contrasena_antigua.place(x=22, y=130)
        # Caja contraseña antigua
        caja_contrasena_antigua = ttk.Entry(interfaz_cambiar_contrasena, font="Helveltica 10", width=26)
        caja_contrasena_antigua.pack()
        caja_contrasena_antigua.place(x=250, y=132)
        # Imagen y label de la linea separadora
        imagen_separador = ImageTk.PhotoImage(file="img/separador.png")
        linea_caja_contrasena_antigua = Label(interfaz_cambiar_contrasena, image=imagen_separador)
        linea_caja_contrasena_antigua.pack()
        linea_caja_contrasena_antigua.place(x=0, y=155)

        # Label nueva contraseña
        label_nueva_contrasena = Label(interfaz_cambiar_contrasena, text="Escriba la nueva contraseña:", font="Helveltica 12")
        label_nueva_contrasena.pack()
        label_nueva_contrasena.place(x=22, y=200)
        # Caja nueva contraseña
        caja_nueva_contrasena = ttk.Entry(interfaz_cambiar_contrasena, font="Helveltica 10", width=26)
        caja_nueva_contrasena.pack()
        caja_nueva_contrasena.place(x=250, y=202)

        # Label repetir nueva contraseña
        label_nueva_contrasena2 = Label(interfaz_cambiar_contrasena, text="Repita la nueva contraseña:", font="Helveltica 12")
        label_nueva_contrasena2.pack()
        label_nueva_contrasena2.place(x=22, y=250)
        # Caja repetir contraseña
        caja_repetir_contrasena = ttk.Entry(interfaz_cambiar_contrasena, font="Helveltica 10", width=26)
        caja_repetir_contrasena.pack()
        caja_repetir_contrasena.place(x=250, y=252)

        # Linea que separa los botones
        canvas = Canvas(interfaz_cambiar_contrasena, bg="#808080", width=650, height=1.5)
        canvas.pack()
        canvas.place(x=0, y=325)


        # Boton aplicar
        boton_aplicar = ttk.Button(interfaz_cambiar_contrasena, text="Aplicar")
        boton_aplicar.pack()
        boton_aplicar.place(x=450, y=340)

        # Funcion para volver al menu
        def volver_cambiar_contrasena():
            interfaz_cambiar_contrasena.destroy()
            menu(usuario, contrasena_principal)
        # Boton cancelar
        boton_cancelar = ttk.Button(interfaz_cambiar_contrasena, text="Cancelar", command=volver_cambiar_contrasena)
        boton_cancelar.pack()
        boton_cancelar.place(x=550, y=340)
        # Si se le da a la X
        interfaz_cambiar_contrasena.protocol("WM_DELETE_WINDOW", volver_cambiar_contrasena)
        interfaz_cambiar_contrasena.mainloop()


    # Funcion para llamar a cambiar_contrasena pasandole la variable de contrasena_principal
    def cambiar_contrasena_ir():
        cambiar_contrasena(contrasena_principal)

    # Imagen y boton para actualizar programa
    imagen_actualizar=ImageTk.PhotoImage(file="img/actualizar_pequena.png")
    boton_actualizar=ttk.Button(interfaz_menu, image=imagen_actualizar, text="Actualizar", command=update, cursor="hand2", compound="left")
    boton_actualizar.pack()
    boton_actualizar.place(x=550, y=285)

    # Defino el menu y se lo añado a la interfaz
    menubar=Menu(interfaz_menu)
    interfaz_menu.config(menu=menubar)
    # Creo el filemenu
    filemenu = Menu(menubar, tearoff=0)
    # Añado el titulo  al menu
    menubar.add_cascade(label="Opciones", menu=filemenu)
    filemenu.add_cascade(label="Cambiar contraseña", command=cambiar_contrasena_ir)


    interfaz_menu.mainloop()
    # Acaba la ventana del menu
    # -------------------------------------------------


# SI ES LA PRIMERA VEZ QUE SE INICIA EL PROGRAMA:
try:
    o=open("primeravez.txt","r")
    o.close() # Cierro la conexion para que se pueda eliminar el archivo
    os.system("del primeravez.txt") # Destruyo el archivo
    os.system("instrucciones.exe")

# Si no es la primeravez
except:
    #---- VENTANA PRINCIPAL DE VALIDACION ----

    #Defino la ventana de validacion
    interfaz_validar=Tk()
    interfaz_validar.title("Validar")
    interfaz_validar.configure(background="#3e8de7")
    bit = interfaz_validar.iconbitmap("img/icono.ico") # Icono de la interfaz
    interfaz_validar.resizable(0,0)
    # Variables del ancho y alto de la interfaz
    ancho_interfaz = 700
    alto_interfaz = 350
    # Formula para centar la interfaz en la pantalla
    windowWidth = interfaz_validar.winfo_reqwidth()  + ancho_interfaz / 2
    windowHeight = interfaz_validar.winfo_reqheight() + alto_interfaz / 2
    # Posiciones de la pantalla
    positionRight = int(interfaz_validar.winfo_screenwidth()/2 - windowWidth/2 )
    positionDown = int(interfaz_validar.winfo_screenheight()/2 - windowHeight/2)
    # Centro la interfaz en la mitad de la pantalla aproximadamente
    interfaz_validar.geometry("700x350+{}+{}".format(positionRight, positionDown))

    # Imagen y label del icono
    imagen_icono=ImageTk.PhotoImage(file="img/icono.png")
    label_imagen_icono=Label(interfaz_validar, image=imagen_icono, bg="#3e8de7")
    label_imagen_icono.pack()
    label_imagen_icono.place(x=45, y=0)

    # Linea que separa la imagen del incio de sesion
    canvas = Canvas(interfaz_validar, bg="black", width=3, height=352)
    canvas.pack()
    canvas.place(x=365, y=-5)

    #Label Usuario
    label_usuario=Label(interfaz_validar,  text="Usuario:", height=2, font=("Helvetica",15), bg="#3e8de7")
    label_usuario.pack()
    label_usuario.place(x=400, y=110)

    #Caja del usuario
    caja_usuario=ttk.Entry(interfaz_validar, font="Helveltica 11")
    caja_usuario.pack()
    caja_usuario.place(x=520, y=125)
    caja_usuario.bind("<Return>", Validar_entry)

    #Label contraseña
    label_contrasena=Label(interfaz_validar, text="Contraseña:", height=2, font=("Helvetica",15), bg="#3e8de7")
    label_contrasena.pack()
    label_contrasena.place(x=400, y=160)

    #Caja contraseña
    caja_contrasena=Entry(interfaz_validar, show="*", font="Helveltica 11")
    caja_contrasena.pack()
    caja_contrasena.place(x=520, y=175)
    caja_contrasena.bind("<Return>", Validar_entry)
    caja_contrasena.bind("<Button-1>", Validar_usuario)

    # Imagen del check del boton validar
    imagen_check=ImageTk.PhotoImage(file="img/check_instrucciones.png")
    # Boton validar
    boton_validar=ttk.Button(interfaz_validar, command=Validar, cursor="hand2", text="Validar", width=19, style="boton_validar.TButton", compound="left", image=imagen_check)
    boton_validar.pack()
    boton_validar.place(x=520, y=220)
    ttk.Style().configure("boton_validar.TButton", background="#007e00", foreground="#007e00", font="Helveltica 10")

    # Compruebo la version del programa en el txt (..\version)
    f = open("..\\version", "r")
    version_txt = f.readline()
    version_txt = version_txt.replace("\n", "")
    f.close()
    # Label de la version del programa
    label_version=Label(interfaz_validar, text="Versión: "+version_txt+"", bg="#3e8de7", font=("Helvetica",12))
    label_version.pack()
    label_version.place(x=550, y=300)

    # Imagen y boton salir
    imagen_boton_salir=ImageTk.PhotoImage(file="img/exit.png")
    boton_salir=ttk.Button(interfaz_validar, command=sys.exit, cursor="hand2", image=imagen_boton_salir)
    boton_salir.pack()
    boton_salir.place(x=590, y=10)


    #Cargo la ventana validar
    interfaz_validar.mainloop()
