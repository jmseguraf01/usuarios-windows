#!/usr/bin/env python
# -*- coding: utf-8 -*-

#------------------------------------------------------------------------------------------
# Programa creado por Juan Miguel Segura Fernandez
# Para contactar : jusefer2@hotmail.es
# LICENCIA GNU
#------------------------------------------------------------------------------------------

"""
INSTALACION DEL PROGRAMA USUARIOS.PY
/ SOLO SE ABRE CUANDO EXISTE EL ARCHIVO primeravez.txt
"""

from Tkinter import *
from Tkinter import Tk
from PIL import Image, ImageTk # pip install Image / # Para importar el modulo de imagenes en WINDOWS
import tkMessageBox, os, time, sqlite3, ttk, hashlib, commands

# Defino la interfaz
interfaz_instrucciones=Tk()
interfaz_instrucciones.title("Instalación")
# Variables del ancho y alto de la interfaz
ancho_interfaz = 700
alto_interfaz = 350
# Formula para centar la interfaz en la pantalla
windowWidth = interfaz_instrucciones.winfo_reqwidth()  + ancho_interfaz / 2
windowHeight = interfaz_instrucciones.winfo_reqheight() + alto_interfaz / 2
# Posiciones de la pantalla
positionRight = int(interfaz_instrucciones.winfo_screenwidth()/2 - windowWidth/2 )
positionDown = int(interfaz_instrucciones.winfo_screenheight()/2 - windowHeight/2)
# Centro la interfaz en la mitad de la pantalla aproximadamente
interfaz_instrucciones.geometry("700x350+{}+{}".format(positionRight, positionDown))
# Icono interfaz
bit = interfaz_instrucciones.iconbitmap("img/instrucciones.ico")
interfaz_instrucciones.resizable(0,0)


# Funcion para crear usuario y contraseña
def registro():
    # Defino el usuario y contraseña introducidos en las cajas
    usuario_nuevo= caja_usuario.get()
    contrasena_nueva= caja_contrasena.get()
    repetir_contrasena= caja_repetir_contrasena.get()

    # Compruebo que las contraseñas coincidan
    if contrasena_nueva == repetir_contrasena and contrasena_nueva != "" and repetir_contrasena != "":
        # --------------------
        # GUARDO EL USUARIO
        # --------------------

        # Leo el txt de los usuarios en modo lectura
        f=open("Users.fsf","r")

        l1=f.next()
        usuario0=f.next()
        usuario1=f.next()
        usuario2=f.next()

        # LE QUITO EL SALTO DE LINEA (\n) PARA QUE NO ME DE ERROR DE QUE NO ES IGUAL AL usuario_validar
        usuario0=usuario0.replace("\n","")
        usuario1=usuario1.replace("\n","")
        usuario2=usuario2.replace("\n","")
        f.close()

        # Encripto el nombre del usuario y lo escribo en el txt
        usuario_nuevo_encriptado = hashlib.md5(usuario_nuevo).hexdigest()
        f2=open("Users.fsf", "w")
        f2.write("%s%s\n%s\n%s\n" %(l1,usuario_nuevo_encriptado, usuario1, usuario2)) # Escribo los datos en el txt
        f2.close()

        # --------------------
        # GUARDO LA CONTRASEÑA
        # --------------------
        # Le cambio el nombre a la base de datos
        os.system("move Datos_Usuarios_vacio.s3db Datos_Usuarios.s3db")
        # Cifro la base de  datos
        os.system("GnuPG\gpg.exe -c --batch --passphrase %s Datos_Usuarios.s3db" %(contrasena_nueva))
        os.system("del Datos_Usuarios.s3db")
        os.system("del primeravez.txt")
        tkMessageBox.showinfo("Exito","Se ha registrado el nuevo usuario, ya puede iniciar sesion.")
        interfaz_instrucciones.destroy()
        # Abro el archivo usuarios.
        os.system("usuarios.exe")

    # Si las contraseñas no coinciden
    else:
        tkMessageBox.showerror("Error.","Las contraseñas no coinciden.")

# Funcion de cuando se le da a enter a la caja_contrasena y caja_repetir_contrasena
def entry_registro(event):
    registro()

# Label del titulo
label_titulo=Label(interfaz_instrucciones, text="Instalación de Usuarios.py", font="Helvetica 15", fg="blue", height=2)
label_titulo.pack()

# Creo un canvas para poner el texto y definir la largura
canvas = Canvas(interfaz_instrucciones,width=400, height=75,)
canvas.pack(fill='both', expand=False)
# Linea de debajo del titulo
canvas.create_line(0, 5, 750, 5, fill="blue")
#canvas.create_line(0, 25, 200, 25)
canvas.create_text(20, 20, anchor="nw", font="Helvetica 11",  width=600,
text="Este programa sirve para guardar los usuarios y las contraseñas de diferentes lugares, ya sean de aplicaciones o paginas web (Facebook, Instagram...). Para ello, necesitas crear un usuario y una contraseña con los que accederás al programa.")

# Label de usuario
label_usuario=Label(interfaz_instrucciones, text="Nombre de usuario:", font="Helveltica 12")
label_usuario.pack()
label_usuario.place(x=20, y=145)

# Caja de usuario
caja_usuario=ttk.Entry(interfaz_instrucciones, font="Helveltica 11")
caja_usuario.pack()
caja_usuario.place(x=180, y=147)

# Label contraseña
label_contrasena=Label(interfaz_instrucciones, text="Contraseña:", font="Helvetica 12")
label_contrasena.pack()
label_contrasena.place(x=20, y=190)

# Caja contraseña
caja_contrasena=ttk.Entry(interfaz_instrucciones, font="Helveltica 10", show="*")
caja_contrasena.pack()
caja_contrasena.place(x=180, y=190)
# Enter
caja_contrasena.bind("<Return>", entry_registro)

# Label repetir contraseña
label_repetir_contrasena=Label(interfaz_instrucciones, text="Repita la contraseña:", font="Helvetica 12 italic")
label_repetir_contrasena.pack()
label_repetir_contrasena.place(x=20, y=240)

# Caja repetir contraseña
caja_repetir_contrasena=ttk.Entry(interfaz_instrucciones, font="Helveltica 10", show="*")
caja_repetir_contrasena.pack()
caja_repetir_contrasena.place()
caja_repetir_contrasena.place(x=180, y=240)
# Cuando se le da al "enter"
caja_repetir_contrasena.bind("<Return>", entry_registro)

# Boton y imagen salir
imagen_x=ImageTk.PhotoImage(file="img/x_instrucciones.png")
boton_salir=ttk.Button(interfaz_instrucciones, cursor="hand2", command=sys.exit, text="  Salir", width=10, style="boton_salir.TButton", image=imagen_x, compound="left")
boton_salir.pack()
boton_salir.place(x=20, y=296)
ttk.Style().configure("boton_salir.TButton", font="Helvetica 10",  foreground="red")


# Boton y imagen para guardar los datos
imagen_check=ImageTk.PhotoImage(file="img/check_instrucciones.png")
boton_registro=ttk.Button(interfaz_instrucciones, cursor="hand2", command=registro, text="   Registrarse", width=20, style="boton_registro.TButton", image=imagen_check, compound="left")
boton_registro.pack()
boton_registro.place(x=180, y=295)
ttk.Style().configure("boton_registro.TButton", font="Helvetica 10")

# Imagen y label de la imagen de instalacion
imagen_instalacion=ImageTk.PhotoImage(file="img/instalacion.png")
label_imagen_instalacion=Label(interfaz_instrucciones, image=imagen_instalacion)
label_imagen_instalacion.pack()
label_imagen_instalacion.place(x=400, y=120)

interfaz_instrucciones.mainloop()
