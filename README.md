# Usuarios.py

- Este almacena, en un base de datos, el Usuario y la Contraseña de un lugar concreto, como por ejemplo Google, Facebook, Instagram...
- Usuarios.py tiene una licencia de Software Libre, GNU GENERAL PUBLIC LICENSE. 
- Creador del programa Juan Miguel Segura Fernandez ( jusefer2@hotmail.es).

![usuarios.py](bin/img/icono.png)

# Instalación
- Para instalar el programa, debe abrir el ejecutable __instrucciones.exe__ que se encuentra dentro de la carpeta __bin__.


